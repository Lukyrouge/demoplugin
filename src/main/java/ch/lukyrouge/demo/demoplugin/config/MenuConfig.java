package ch.lukyrouge.demo.demoplugin.config;

import ch.lukyrouge.demo.demoplugin.DemoPlugin;
import ch.lukyrouge.demo.demoplugin.menu.ConfigurableMenu;
import org.apache.commons.io.FilenameUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

public class MenuConfig {

    private final File configFolder;

    private List<FileConfiguration> configs;

    private final DemoPlugin plugin;

    public MenuConfig(DemoPlugin plugin) {
        this.plugin = plugin;
        configFolder = new File(plugin.getDataFolder().getPath() + File.separator + "menus");
        reloadConfig();

    }

    public void reloadConfig() {
        createDefault();
        configs = new ArrayList<>();

        File[] files = Objects.requireNonNull(configFolder.listFiles());
        if (files.length > 0) {
            for (File f : files) {
                plugin.getLogger().info(FilenameUtils.getBaseName(f.getName()));
                if (f.isFile() && FilenameUtils.getExtension(f.getName()).equalsIgnoreCase("yml")) {
                    YamlConfiguration config = YamlConfiguration.loadConfiguration(f);
                    configs.add(config);
                    ConfigurableMenu menu = new ConfigurableMenu(config);
                    DemoPlugin.getMenuManager().addMenu(menu, FilenameUtils.getBaseName(f.getName()));
                    plugin.getLogger().log(Level.INFO, "Registered menu");
                }
            }
        } else plugin.getLogger().log(Level.INFO, "No menu found !");
        File defaultFile = new File(plugin.getDataFolder().getPath() + File.separator + "menus", "defaultmenu.yml");

        if (!defaultFile.exists()) {
            // Charger le default
            Reader defMenuStream = new InputStreamReader(Objects.requireNonNull(plugin.getResource("menus/defaultmenu.yml")),
                    StandardCharsets.UTF_8);
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defMenuStream);
            plugin.saveResource("menus/defaultmenu.yml", false);
            ConfigurableMenu menu = new ConfigurableMenu(defConfig);
            DemoPlugin.getMenuManager().addMenu(menu, "defaultmenu");
        }
    }

    private void createDefault() {
        if (!plugin.getDataFolder().exists()) plugin.getDataFolder().mkdir();
        if (!configFolder.exists()) configFolder.mkdir();
    }
}
