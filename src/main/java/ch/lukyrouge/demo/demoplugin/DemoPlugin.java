package ch.lukyrouge.demo.demoplugin;

import ch.lukyrouge.demo.demoplugin.config.MenuConfig;
import ch.lukyrouge.demo.demoplugin.menu.Menu;
import ch.lukyrouge.demo.demoplugin.menu.MenuManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class DemoPlugin extends JavaPlugin implements Listener {

    private static MenuManager menuManager;

    @Override
    public void onEnable() {
        // Plugin startup logic
        getMenuManager();
        MenuConfig menuConfig = new MenuConfig(this);
        getCommand("menu").setExecutor((sender, command, label, args) -> {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                getMenuManager().openMenu(p, "defaultmenu");
            }
            return true;
        });
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        MenuManager.playerMenus.clear();
    }

    public static MenuManager getMenuManager() {
        if (menuManager == null) menuManager = new MenuManager();
        return menuManager;
    }

    @EventHandler
    public void InventoryClick(InventoryClickEvent e) {
        Player ply = (Player) e.getWhoClicked();

        Menu menu = MenuManager.playerMenus.get(ply);
        if (e.getCurrentItem() == null) return;
        if (menu != null) {
            if (menu.getAction(e.getSlot()) != null) {
                menu.getAction(e.getSlot()).click(ply);
                e.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void inventoryClose(InventoryCloseEvent e) {
        Player ply = (Player) e.getPlayer();
        getMenuManager().closeMenu(ply);
    }
}
