package ch.lukyrouge.demo.demoplugin.menu;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class MenuManager {

    public static HashMap<String, Menu> menus;
    public static HashMap<Player, Menu> playerMenus;

    public MenuManager() {
        if (menus == null) menus = new HashMap<>();
        if (playerMenus == null) playerMenus = new HashMap<>();
    }

    public void addMenu(Menu m, String title) { menus.put(title, m); }

    public Menu getMenu(String name) {
        return menus.getOrDefault(name, null);
    }

    public void openMenu(Player p, String name) {
        Menu m = getMenu(name);
        if (m != null) {
            p.openInventory(m.getInv());
            playerMenus.put(p, m);
        }
    }

    public void closeMenu(Player p) {
        playerMenus.remove(p);
    }
}
