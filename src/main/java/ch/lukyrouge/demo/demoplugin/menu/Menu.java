package ch.lukyrouge.demo.demoplugin.menu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public abstract class Menu {

    private final int slots;
    private Inventory inv;
    private HashMap<Integer, AbstractAction> actions = new HashMap<>();

    public Menu(int slots) {
        this(slots, "");
    }

    public Menu(int slots, String title) {
        this.slots = slots;
        inv = Bukkit.createInventory(null, slots, title);
    }

    public void setItem(ItemStack item, int slot, AbstractAction action) {
        inv.setItem(slot, item);
        actions.put(slot, action);
    }

    public void setItem(ItemStack item, int slot, AbstractAction action, String displayName) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName);
        item.setItemMeta(meta);
        setItem(item, slot, action);
    }

    public void setItem(ItemStack item, int slot, AbstractAction action, String displayName, List<String> lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        item.setItemMeta(meta);
        setItem(item, slot, action);
    }

    public Inventory getInv() {
        return inv;
    }

    public AbstractAction getAction(int slot) {
        return actions.get(slot);
    }

    public interface AbstractAction {
        void click(Player p);
    }
}
