package ch.lukyrouge.demo.demoplugin.menu;

import ch.lukyrouge.demo.demoplugin.DemoPlugin;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ConfigurableMenu extends Menu {
    private final YamlConfiguration config;

    public ConfigurableMenu(YamlConfiguration config) {
        super(config.getInt("slots"), config.getString("title"));
        this.config = config;
        setupItems();
    }

    private void setupItems() {
        Set<String> section = Objects.requireNonNull(config.getConfigurationSection("buttons")).getKeys(false);
        for (String s : section) {
            int slot = Integer.parseInt(s);
            String name = config.getString("buttons." + s + ".name");
            int count = config.getInt("buttons." + s + ".count");
            String actionType = config.getString("buttons." + s + ".actionType");
            String action = config.getString("buttons." + s + ".action");
            List<String> lore = config.getStringList("buttons." + s + ".lore");
            String materiaString = config.getString("buttons." + s + ".material");

            assert materiaString != null;
            Material material = Material.getMaterial(materiaString);
            ItemStack item = new ItemStack(material == null ? Material.WHITE_STAINED_GLASS_PANE : material);
            item.setAmount(count);
            assert actionType != null;
            setItem(item, slot, processAction(actionType, action), name, lore);
        }
    }

    private static String processActionString(String action, Player p) {
        if (action.contains("[player]")) action.replace("[player]", p.getName());

        return action;
    }

    private static AbstractAction processAction(String actionType, String action) {
        AbstractAction a = null;

        switch (actionType) {
            case "command":
                a = p -> p.performCommand(processActionString(action, p));
                break;
            case "menu":
                a = p -> DemoPlugin.getMenuManager().openMenu(p, action);
                break;
            case "message":
                a = p -> p.sendMessage(action);
                break;
        }

        return a;
    }
}
